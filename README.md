# fromScratch

## Webhook
### Liens

* Creation de webhook :
https://leovoel.github.io/embed-visualizer/

* Couleurs pour la bordure
https://fr.spycolor.com/

un exemple

```
{
  "embeds": {
    "title": "Google it!",
    "url": "https://www.google.com/search?q=RECHERCHE"
  }
}
```

```js
{
  "embed": {
    "title": "Joueur",
    "description": "INFOS.\nINFOS. ```CARAC```",
    "color": 5522111,
    "footer": {
      "text": "PLAYER"
    },
    "thumbnail": {
      "url": "https://cdn.discordapp.com/embed/avatars/0.png"
    },
    "fields": [
      {
        "name": "TITLE",
        "value": "infos"
      },
      {
        "name": "TITLE1",
        "value": "infos",
        "inline": true
      },
      {
        "name": "TITLE2",
        "value": "infos",
        "inline": true
      }
    ]
  }
}
```
