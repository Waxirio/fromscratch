/*
 author : Samuel PELSY-MOZIMANN
 creation du projet : 06/04/2019
 derniere modification : 06/04/2019
*/

// ***** LIBS *****
const Discord = require('discord.js');
const schedule = require('node-schedule');
const fs = require("fs");

// ***** CHARGEMENT DES FICHIERS JSON *****
const settings = require("./settings.json");
// ***** CHARGEMENT DES PARAMETRES *****
const prefix = settings.prefix;
const mainChan = settings.mainChan;
// ***** CHARGEMENT DU CLIENT *****
const bot = new Discord.Client();
bot.commands = new Discord.Collection();
// ******************** Code des evenements de statut du bot ********************

//Quand le client est opérationnel
bot.on('ready', () => {
  console.log(`Connexion terminée : ${bot.user.tag}\n`);

  const CronJob = require('cron').CronJob;
  console.log('Lancement du cron');
  const job = new CronJob('0 0 17 * * *', function() {
  	const d = new Date();
  	resetAndDisplay();
  });
  job.start();
});

function init(){
  // ** INIT DU BOT **
  console.log("recuperation de la cle d'authentification... \n");
  var authLogin = fs.readFileSync("auth.json").token;
  console.log("authentification...");
  //
  // ** LOGIN **
  bot.login("NTY0MDM4NDY2Nzg3MDgyMjUw.XKiElw.1VRH3t6lPJxVIi0FpnkSdNbfzuc");
}

/*
  Lorsque l'on reçoit un message dans un channel
  Entree : message provenant de discord
*/
bot.on('message', async msg => {
  //Si le message est le sien, pas de boucle
  if(msg.author.bot) return;
  //Si c'est dans un message prive, on fait R
  //if(msg.channel.type === "dm") return;

  let msgTab = msg.content.split(/\s+/g); //Tableau des elements du message
  let cmd = msgTab[0]; //commande de base (premier element de la commande)
  let args = msgTab.slice(1) //Arguments de la commande (après la commande de base)

  //Si la commande commence pas par le prefix
  if(!cmd.startsWith(prefix)) return;

  if(cmd === prefix + "report"){
    report(bot, msg, args);
  }
  if(cmd === prefix + "report"){
    report(bot, msg, args);
  }

});

// ****************************** FILE RESET ******************************

function report(bot, msg, args){
  //On donne les donnees pour le fichier
  const fs = require('fs');
  var file = "./data/report.json";
  var user = msg.author;
  //On choppe la commande
  var arg = args[0];
  args.shift();
  //On met en place les valeurs par default de notre appli
  var text = "Usage : Report <add/remove/clear/list> <list>";
  d = new Date();
  var footer_text = d.toTimeString();
  footer_text = footer_text.split(' ')[0];
  color = 10197915;
  var title = "report command";

  if(arg == "add"){ // *************** ADD ***************
    //On recupere le texte derrière
    var res = "";
    args.forEach((ele, i) =>{
      res += ele + " ";
    })
    //On choppe les elements du fichier file
    let repList = JSON.parse(fs.readFileSync(file, "utf8"));
    //On check si l'utilisateur est deja dedans ou non
    repList = checkValidity(repList,user, file, fs);

    //On ajoute le nouvel element a la liste
    repList[user.id].element.push(res);
    fs.writeFile(file, JSON.stringify(repList), (err) => {
      if(err) console.log("ERR: Add element");
    });

    //On ajuste la couleur et le text final
    title = "Add command";
    text = "New element added successfully ! :ballot_box_with_check:  ";
    color = 57497;
  }
  else if(arg == "remove"){ // *************** REMOVE ***************
    //Si l'argument est bien un nombre
    if(isNumber(args[0])){
      //On arrondi si des cons donnent des nombres a virgule
      args[0] = Math.round(args[0]);
      //On choppe les elements du fichier file
      let repList = JSON.parse(fs.readFileSync(file, "utf8"));
      //On check si l'utilisateur est deja dedans ou non
      repList = checkValidity(repList,user, file, fs);
      var userList = repList[user.id].element;

      if(args[0] < userList.length && args[0] >= 0){
        //On balance l'element souhaité
        repList[user.id].element.splice(args[0],1);

        //On ajoute le nouvel element a la liste
        fs.writeFile(file, JSON.stringify(repList), (err) => {
          if(err) console.log("ERR: Remove element");
        });

        //On ajuste la couleur et le text final
        title = "Remove command";
        text = "Element successfully removed ! :ballot_box_with_check: ";
      }
      else{
        //On ajuste la couleur et le text final
        text = "I need a valid id !";
      }
    }
    else{
      //On ajuste la couleur et le text final
      text = "I need a number for argument";
    }
      color = 14680064;
  }
  else if(arg == "list"){ // *************** INFO ***************
    let repList = JSON.parse(fs.readFileSync(file, "utf8"));
    //On check si l'utilisateur est deja dedans ou non
    repList = checkValidity(repList,user, file, fs);
    var userList = repList[user.id].element;

    text = "";

    for(let i=0; i<userList.length; i++){
      text += userList[i] + "\n";
    }
    text += "\n" + userList.length + "/" + userList.length + "\n";

    //On ajuste la couleur et le text final
    title = "List command";
    color = 2760703;
  }
  else if(arg == "clear"){ // *************** CLEAR ***************
    let repList = JSON.parse(fs.readFileSync(file, "utf8"));
    //On check si l'utilisateur est deja dedans ou non
    repList = checkValidity(repList,user, file, fs);
    var userList = repList[user.id].element;
    //On balance tout
    repList[user.id].element.splice(args[0],userList.length);

    //On ajoute le nouvel element a la liste
    fs.writeFile(file, JSON.stringify(repList), (err) => {
      if(err) console.log("ERR: Clear elements");
    });

    //On ajuste la couleur et le text final
    title = "Clear command";
    text = "All elements successfully cleared ! :ballot_box_with_check: "
    color = 16759552;
  }

  let embed = {
                "embed": {
                  "title": title,
                  "color": color,
                  "description": text,
                  "footer": {
                    "text": footer_text
                  },
                  "author": {
                    "name": bot.tag
                  }
                }
              }

  msg.channel.send(embed);
}

//On check si l'utilisateur est existant, sinon on le crée
function checkValidity(repList, user, file, fs){
  //Si l'utilisateur n'est pas dedans
  if(!repList[user.id]){
    console.log("USER : Creating new profile");
    repList[user.id] = {
        "name": user.tag,
        "element": [] //On l'ajoute
    }
  }
  return repList;
}

//Si c'est un nombre on revoie vrai, faux sinon
function isNumber(n){
  return (! isNaN(n));
}

async function resetAndDisplay(){
  console.log("Reset and display");
  const fs = require("fs");
  let repList = JSON.parse(fs.readFileSync("./data/report.json", "utf8"));
  for (key in repList) {
    console.log(key);
    d = new Date();
    var footer_text = d.toTimeString();
    footer_text = footer_text.split(' ')[0];
    color = 2555648;
    var title = "Daily Report " + repList[key].name ;

    var userList = repList[key].element;

    text = "";
    for(let i=0; i<userList.length; i++){
      text += "• " + userList[i] + "\n";
    }

    let embed = {
                  "embed": {
                    "title": title,
                    "color": color,
                    "description": text,
                    "footer": {
                      "text": footer_text
                    },
                    "author": {
                      "name": bot.tag
                    }
                  }
                }
    //console.log("hehehehehehe");
    bot.channels.get(mainChan).send(embed);

    repList[key].element.splice(args[0],userList.length);

    //On ajoute le nouvel element a la liste
    fs.writeFile(file, JSON.stringify(repList), (err) => {
      if(err) console.log("ERR: Clear all");
    });
  }
}

// *************** RESET EVERY 5 hours ***************

async function resetForReport() {
    var now = new Date();
    var tomorrow = new Date(
        now.getYear(),
        now.getMonth(),
        now.getDay(),// + 1
        11,
        46,
        0
    );
    var msToTomorrow= tomorrow.getTime() - now.getTime();

    setTimeout(function() {
        resetAndDisplay();
        resetForReport();
    }, msToTomorrow);

  fs.writeFile("./data/report.json", "{}", (err) => {
    if(err) console.log("ERR: Clear and display elements");
  });
}

// *************** RESET COMMANDS ***************

function report(bot, msg, args){
//On donne les donnees pour le fichier
  var file = "./data/report.json";
  var user = msg.author;
  //On choppe la commande
  var arg = args[0];
  args.shift();
  //On met en place les valeurs par default de notre appli
  var text = "Usage : Report <add/remove/clear/list> <list>";
  d = new Date();
  var footer_text = d.toTimeString();
  footer_text = footer_text.split(' ')[0];
  color = 10197915;
  var title = "report command";

  if(arg == "add"){ // *************** ADD ***************
    //On recupere le texte derrière
    var res = "";
    args.forEach((ele, i) =>{
      res += ele + " ";
    })
    //On choppe les elements du fichier file
    let repList = JSON.parse(fs.readFileSync(file, "utf8"));
    //On check si l'utilisateur est deja dedans ou non
    repList = checkValidity(repList,user, file, fs);

    //On ajoute le nouvel element a la liste
    repList[user.id].element.push(res);
    fs.writeFile(file, JSON.stringify(repList), (err) => {
      if(err) console.log("ERR: Add element");
    });

    //On ajuste la couleur et le text final
    title = "Add command";
    text = "New element added successfully ! :ballot_box_with_check:  ";
    color = 57497;
  }
  else if(arg == "remove"){ // *************** REMOVE ***************
    //Si l'argument est bien un nombre
    if(isNumber(args[0])){
      //On arrondi si des cons donnent des nombres a virgule
      args[0] = Math.round(args[0]);
      //On choppe les elements du fichier file
      let repList = JSON.parse(fs.readFileSync(file, "utf8"));
      //On check si l'utilisateur est deja dedans ou non
      repList = checkValidity(repList,user, file, fs);
      var userList = repList[user.id].element;

      if(args[0] <= userList.length && args[0] >= 0){
        //On balance l'element souhaité
        repList[user.id].element.splice(args[0],1);

        //On ajoute le nouvel element a la liste
        fs.writeFile(file, JSON.stringify(repList), (err) => {
          if(err) console.log("ERR: Remove element");
        });

        //On ajuste la couleur et le text final
        title = "Remove command";
        text = "Element successfully removed ! :ballot_box_with_check: ";
      }
      else{
        //On ajuste la couleur et le text final
        text = "I need a valid id !";
      }
    }
    else{
      //On ajuste la couleur et le text final
      text = "I need a number for argument";
    }
      color = 14680064;
  }
  else if(arg == "list"){ // *************** INFO ***************
    let repList = JSON.parse(fs.readFileSync(file, "utf8"));
    //On check si l'utilisateur est deja dedans ou non
    repList = checkValidity(repList,user, file, fs);
    var userList = repList[user.id].element;

    text = "";

    for(let i=0; i<userList.length; i++){
      text += userList[i] + "\n";
    }
    text += "\n" + userList.length + "/" + userList.length + "\n";

    //On ajuste la couleur et le text final
    title = "List command";
    color = 2760703;
  }
  else if(arg == "clear"){ // *************** CLEAR ***************
    let repList = JSON.parse(fs.readFileSync(file, "utf8"));
    //On check si l'utilisateur est deja dedans ou non
    repList = checkValidity(repList,user, file, fs);
    var userList = repList[user.id].element;
    //On balance tout
    repList[user.id].element.splice(args[0],userList.length);

    //On ajoute le nouvel element a la liste
    fs.writeFile(file, JSON.stringify(repList), (err) => {
      if(err) console.log("ERR: Clear elements");
    });

    //On ajuste la couleur et le text final
    title = "Clear command";
    text = "All elements successfully cleared ! :ballot_box_with_check: "
    color = 16759552;
  }

  let embed = {
                "embed": {
                  "title": title,
                  "color": color,
                  "description": text,
                  "footer": {
                    "text": footer_text
                  },
                  "author": {
                    "name": "fromScratch"
                  }
                }
              }

  msg.channel.send(embed);
}

//On check si l'utilisateur est existant, sinon on le crée
function checkValidity(repList, user, file, fs){
  //Si l'utilisateur n'est pas dedans
  if(!repList[user.id]){
    console.log("USER : Creating new profile");
    repList[user.id] = {
        "name": user.tag,
        "element": [] //On l'ajoute
    }
    //Puis on l'écrit dans le fichier
    fs.writeFile(file, JSON.stringify(repList), (err) => {
        if(err) console.log("ERR: Check Validity");
    });
  }
  return repList;
}

//Si c'est un nombre on revoie vrai, faux sinon
function isNumber(n){
  return (! isNaN(n));
}

// ******************** On initialise le bot ********************
init();
